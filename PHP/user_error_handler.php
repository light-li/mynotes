<?php
error_reporting( 0 );
function errorHandler($errno, $errmsg, $errfile, $errline, $errcontext = array()/*发生错误时的活动符号表*/) {
	$msg_arr = array(
		'[' . date('Y-m-d H:i:s') . ']',
		$errmsg,
		'|',
		$errfile,
		'line:' . $errline
	);
	$err_msg = implode(' ', $msg_arr);
	echo $err_msg;
	//$res = error_log( $err_msg . PHP_EOL, 3, 'error.log', 'extra');
	$fp = fopen('error.log', 'a+');
	fwrite($fp, $err_msg . PHP_EOL);
	fclose($fp);
	return false;
}
//exception无法记录http://www.laruence.com/2010/08/03/1697.html
function fatalErrorHandler() {
	$e = error_get_last();
	var_dump($e);
	switch($e['type']) {
		case E_ERROR:
		case E_PARSE:
		case E_CORE_ERROR:
		case E_COMPILE_ERROR:
		case E_USER_ERROR:
			errorHandler($e['type'], $e['message'], $e['file'], $e['line']);
			break;
		default:
			break;
	}
}
function exceptionHandler($exception){
	var_dump($exception);
	errorHandler($exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
}
/**
*	第二个参数指定的错误类型都会绕过PHP的标准错误处理程序，除非回调函数返回了false
*	如果回调函数返回的为true，则会跳过PHP的标准错误处理程序
**/
set_error_handler('errorHandler');
set_exception_handler('exceptionHandler');
/*将无法用用户定义的函数来处的错误流入处理函数*/
register_shutdown_function('fatalErrorHandler');


class Test {
	public function index() {
		$test = 'test';
		
		echo $undefinedVarible;
		//throw new Exception('dasd');
	}
}

$test = new Test();
$test->index();
$test = new Tesdt();
$test->index();