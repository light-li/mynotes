<?php
/**
 *	根据生日来计算年龄
 *
 * @param string $date 生日日期
 * @return string 年龄
*/
function culAgeByBirthday($date){
	$year_diff = '';
	$time = strtotime($date);
	if(FALSE === $time){
		return '';
	}

	$date = date('Y-m-d', $time);
	list($year, $month, $day) = explode("-", $date);
	//$year_diff = date("Y") – $year;
	//$month_diff = date("m") – $month;
	//$day_diff = date("d") – $day;
	if ($day_diff < 0 || $month_diff < 0) $year_diff–;

	return $year_diff;
}