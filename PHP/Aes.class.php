<?php
/**
*	@Rijndael加密法
**/
class Aes{
	public static function Encrypt($data,$key){
		$decodeKey = base64_decode($key);
		$iv     = substr($decodeKey,0,16);
		$encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $decodeKey, $data, MCRYPT_MODE_CBC, $iv); 

		return $encrypted;
	}

	public static function Decrypt($data, $key){
		$decodeKey = base64_decode($key);
		$iv     = substr($decodeKey,0,16);
		$encrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $decodeKey, $data, MCRYPT_MODE_CBC, $iv); 

		return $encrypted;
	}
}
//加密key
$key = '5D8EC9EE0E164090A25E6F1F';

$data = Aes::Encrypt( 'hello world', $key );

echo base64_encode( $data );
echo '<br />';
echo  Aes::Decrypt( $data, $key );